---
layout: default
title: "How to engage with the team"
---
# How to engage with the team

## Contribute

You love Zanshin so much that you want to contribute to it? That's easy!
Zanshin is a [KDE](http://www.kde.org) Application and as such is hosted
on the KDE Git infrastructure.

Simply visit our [project page][project], clone the [repository][repo] and send
patches our way right away! We review them using [Gitlab][review].

[project]: https://phabricator.kde.org/tag/zanshin/
[repo]: https://invent.kde.org/pim/zanshin
[review]: https://invent.kde.org/pim/zanshin/-/merge_requests

## Ask questions

You can find the developers and some of the most active and
enthusiastic users on IRC. The channel is [#zanshin](irc://irc.freenode.net/#zanshin),
on the [Freenode](http://www.freenode.net) network.

You can also ask questions through the comment system of our
[announcements](/news).

## Report bugs

We use the [KDE Bugzilla](http://bugs.kde.org) to deal with bug reports.
Before reporting, please make sure your issue isn't already in
the [list of open issues](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&product=zanshin).



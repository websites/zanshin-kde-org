---
layout: default
title: "How to Install"
---

<h4>Zanshin is available for a variety of operating systems. You will find below how
to get your task management fix!</h4>
<br/><br/>

<p>
<span class="image left"><img src="{{ site.base_url }}/images/windows.png"/></span>
Although we didn't test it ourselves on Windows, Zanshin is available thanks to
the <a href="http://windows.kde.org">KDE Windows</a> installer. Just download the
installer, and go through the wizard while making sure to check Zanshin in the list of
components to install, that should get you started.</p>
<br/><br/><br/>

<p>
<span class="image left"><img src="{{ site.base_url }}/images/macosx.png"/></span>
It is not available yet, but don't hesitate to <a href="/join">join us</a> to help us getting
it working for Mac users.</p>
<br/><br/><br/><br/><br/>

<p>
<span class="image left"><img src="{{ site.base_url }}/images/linux.png"/></span>
Zanshin is also available for Linux based systems. Used with other KDE Applications
or within the Plasma Desktop, it unlocks all of its best features, try it out!

<ul>
  <li>On KDE Neon, it is directly available and up to date;</li>
  <li>On OpenSUSE, it is directly available for all releases since Leap 42.3, more recent versions are also available through the home:ervin and KDE:Extra repositories;</li>
  <li>On Fedora, it is directly available for all releases since Fedora 15;</li>
  <li>On Ubuntu, it is directly available in universe for all releases since Ubuntu 12.04;</li>
  <li>On Arch Linux, it is directly available in the community repository;</li>
  <li>On Gentoo, it is directly available through the package simply named zanshin.</li>
</ul>

Your distribution is missing? Don't hesitate to <a href="/join">join us</a> to help up getting
it working on your distribution.</p>

And of course, you can also [download the sources](http://download.kde.org/stable/zanshin/)!


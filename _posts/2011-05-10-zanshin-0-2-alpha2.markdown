---
date: '2011-05-10 01:07:45'
layout: post
title: Zanshin 0.2 alpha2
tags:
- Akonadi
- KDE
- Zanshin
---

After quite some work on stabilizing, testing the core, and adding
some extra features, I'm happy to announce that I just tagged
Zanshin 0.2 alpha2!

The big highlight of this alpha is the availability of a new
krunner plugin, so that you can easily collect todos even when
Zanshin isn't running. Bring up krunner, type in "todo: buy
apples", and the newly created todo will be waiting for you in your
inbox the next time you look at your Zanshin window. Collect from
anywhere on your desktop now!

We also added an extra dialog to configure Akonadi resources which
is displayed on first run, and better defaults for the columns and
window sizes, which should provide a smoother experience for new
users. And of course it comes with more automated tests, and
bugfixes.

If everything goes well, it should be our last alpha, and we should
proceed with 0.2 beta1 next. For those interested, it is available
for openSUSE in my
[home:ervin repository](http://download.opensuse.org/repositories/home:/ervin/ "home:ervin openSUSE repository").
For the people wanting to build from sources, it is still a git
clone kde:zanshin away.

I'd like to thanks Mario Bensi and Benjamin Port who have been
fearless bug hunters for that release. Way to go guys!

PS: As mentionned, I package it for openSUSE myself as it is my
distro of choice, but we're obviously looking for packagers
targetting other distributions. If you're already working on such
packages, or are willing to work on them, please get in touch with
me for improving synchronization toward the 0.2 release.




---
date: '2011-02-07 17:07:43'
layout: post
title: Zanshin 0.2 alpha1
tags:
- Akonadi
- KDE
- PIM
- Zanshin
---

Some people might remember that I was rambling a while back about a
TODO management application named Zanshin. It even has a few
users... they have probably been wondering why it got stuck at this
mysterious non advertised 0.1 version.

Don't fear anymore dear users, Zanshin is not dead, it is pretty
much alive, and we just tagged 0.2 alpha1 today!

It took us a while, we had to rewrite quite some bits in order to
benefit from the new additions of the Akonadi ecosystem we would
have missed otherwise. So we're back, and we plan at least one more
alpha, before going in the beta cycle. It is your chance to give us
feedback early on to get a solid 0.2 release.

Of course it is an alpha, so it might not suit you for production
use... Personally I switched to it in production and it didn't burn
my home yet. It will soon be available for openSUSE in my
[home:ervin repository](http://download.opensuse.org/repositories/home:/ervin/ "home:ervin openSUSE repository"),
once it gets out of the build farm (already the case for factory,
not yet for 11.3). If you're building from sources, it's only a
"git clone kde:zanshin.git" away (we're actually among the first
projects to migrate there).

I'd like to give a big kudo to Mario Bensi, who is working with me
on Zanshin. He did a tremendous job on that alpha. For the last
month I've been mostly giving architecture directions and reviewing
patches... still I had difficulties to keep up with the patch
stream he was sending my way. Great job Mario!

PS: As mentionned, I package it for openSUSE myself as it is my
distro of choice, but we're obviously looking for fearless
packagers targetting other distributions.




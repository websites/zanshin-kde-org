---
layout: post
title: "Zanshin 0.2 beta2"
date: Wed Aug 31 17:54:49 +0200 2011
tags:
- Akonadi
- KDE
- Zanshin
---

Zanshin, the TODO application which helps keeping your mind uncluttered
is back! After one month of waiting, we are delighted to announce the
immediate availability of Zanshin 0.2 beta2!

The focus has been mainly on bugfixing, but we also did a couple of
usability adjustments here and there. Also, thanks to the awesome Nuno
Pinheiro, we have an application icon (previously we were just hijacking
KOrganizer icon). This new icon is lovely, thanks Nuno!

The source tarballs are available, at the same place than the last time on
[files.kde.org](http://files.kde.org/zanshin "Zanshin tarballs"). If you want
to use it on openSUSE
[my repository](http://download.opensuse.org/repositories/home:/ervin/ "home:ervin openSUSE repository")
has a package for Zanshin, but it's now also available on the
[KDE:Unstable:Playground repository](http://download.opensuse.org/repositories/KDE:/Unstable:/Playground/ "KDE:Unstable:Playground repository").
Last but not least! Zanshin is also now packaged for Gentoo. Thank you to
Matija Suklje for working on it.

And of course, you can still *git clone kde:zanshin* if you want the bleeding
edge or if you wish to contribute to the code.

You can also contribute by helping us reaching more users:

  - packaging Zanshin for your distro, we still miss big ones like Arch, Fedora
    or Debian/Ubuntu;
  - making sure Zanshin runs on MS Windows, apparently Patrick from the KDE-Windows
    team was toying with that during DS but I'm not sure how it went;
  - or making sure it runs on Mac OS (we're not aware of any effort on that
    platform yet).

Now we're relaxing a bit, and waiting for feedback to see what needs fixing
for the next release. Depending on the defect rate next one could be 0.2 rc1.
Looks like we're getting closer and closer from 0.2 final!


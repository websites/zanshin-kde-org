module Jekyll
  class ArchiveListTag < Liquid::Tag
    safe = true

    def initialize(tag_name, params, tokens)
      super
    end

    def render(context)
      grouped_posts = self.collate_by_year_and_month(context.registers[:site].posts)
      base_url = context.registers[:site].config["base_url"]
      html = "\n<table id=\"archive_list\">\n"
      grouped_posts.sort{|y1,y2| y1[0].to_i <=> y2[0].to_i }.reverse_each do |year, year_posts|
        html << "\t<tr class=\"archive_year\"><td>#{year}</td><td></td></tr>\n"
        year_posts.sort{|m1,m2| m1[0].to_i <=> m2[0].to_i }.reverse_each do |month, posts|
          month_name = Date::MONTHNAMES[month.to_i]
          html << "\t<tr class=\"archive_month\"><td>#{month_name}</td><td></td></tr>\n"
          posts.reverse_each do |post|
            day = post.date.day
            url = base_url+post.url
            title = post.data["title"]
            html << "\t<tr><td class=\"archive_day\">#{day}</td><td><a href=\"#{url}\">#{title}</a></td></tr>\n"
          end
          html << "\n"
        end
        html << "\n\n"
      end
      html << "</table>\n"
      html
    end

    def collate_by_year_and_month(posts)
      collated = {}
      posts.each do |post|
        year_key = "#{post.date.year}"
        month_key = "#{post.date.month}"
        if collated.has_key? year_key
          if collated[year_key].has_key? month_key
            collated[year_key][month_key] << post
          else
            collated[year_key][month_key] = [post]
          end
        else
          collated[year_key] = {}
          collated[year_key][month_key] = [post]
        end
      end
      collated
    end
  end
end

Liquid::Template.register_tag('archive_list', Jekyll::ArchiveListTag)
